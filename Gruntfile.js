var path = require('path');
module.exports = function (grunt) {

    grunt.initConfig({
        bower_concat: {
            all: {
                dest: 'public/javascripts/bower.js',
                cssDest: 'public/stylesheets/bower.css'
            }
        },
        express: {
            dev: {
                options: {
                    script: './bin/www.js',
                    port: 3000

                }
            }
        },
        stylus: {
            compile: {
                options: {
                    paths: [
                        "node_modules/grunt-contrib-stylus/node_modules",
                        "node_modules/jeet/stylus"
                    ]
                },
                files: {
                    'public/stylesheets/style.css': ['public/stylesheets/*.styl'] // compile and concat into single file
                }
            }
        },
        watch: {
            stylus: {
                files: 'public/stylesheets/*.styl',
                tasks: 'stylus'
            },
            express: {
                files: ['views/*.hbs'],
                tasks: ['express'],
                options: {
                    nospawn: false
                }
            },
            options:{
                livereload:true
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-stylus');
    grunt.loadNpmTasks('grunt-express-server');
    grunt.loadNpmTasks('grunt-bower-concat');


    grunt.registerTask('default', ['bower_concat', 'stylus', 'express', 'watch']);

};
